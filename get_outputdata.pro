; Purpose:
; read GC-classic and GCHP's output data 
;
; Function description:
; getdata_LL: get the data of all tracers at TAU0 from CC-classic's ND49 bpch diagnostics. tracername is an output array that will be used in many other places. To be called by GCHP_benchmark.pro. 
; getdata_CS: get the data from GCHP's output. tracername is an input array obtained by getdata_LL. This ensure data_LL and data_CS have the same ordering of tracers.  
;
; Notes:
; variable naming: the suffix "_LL" (lat-lon) stands for CC-classic and "_CS" (cube-sphere) stands for GCHP. In the future python version, such difference is expected to be removed because there will be only one I/O routine dealing with NetCDF file.
;

pro getdata_LL,tracername,data_LL,file_LL,time_LL

   ; read GC-classic ND49 time series
   CTM_GET_DATA, datainfo, FILENAME=file_LL,TAU0=time_LL

   Ntracer =n_elements(datainfo)
   Nlon = datainfo[0].dim[0]
   Nlat = datainfo[0].dim[1]
   Nlev= datainfo[0].dim[2]

   tracername = strarr(Ntracer)
   data_LL = fltarr(Nlon,Nlat,Nlev,Ntracer)

   for n=0,Ntracer-1 do begin
       tracername[n] = datainfo[n].tracername
       data_LL[*,*,*,n] = *(datainfo[n].data)
   endfor

end ; end of program

pro getdata_CS,tracername,data_CS,file_CS,time_CS
   ; read GCHP output time series
   fid = NCDF_OPEN( file_CS,/nowrite ) ; read-only

   Ntracer = n_elements(tracername)
   NCDF_DIMINQ, fid, 0, dimname, Nlon 
   NCDF_DIMINQ, fid, 1, dimname, Nlat
   NCDF_DIMINQ, fid, 2, dimname, Nlev
   NCDF_DIMINQ, fid, 3, dimname, Ntime 

   data_CS = fltarr(Nlon,Nlat,Nlev,Ntracer)

   for n=0,Ntracer-1 do begin 
       ;construct the tracer name string in GCHP. e.g.'TRC_NO'
       varname='TRC_'+tracername[n] 

       if Ntime eq 1 then begin 
       ; only one time slice. IDL will read the data as 3D variable  
           data_temp = NCDF_GET( fid, varname )
       endif else begin
       ; more than one time slice. Only get time_CS 
           data_temp4D = NCDF_GET( fid, varname )
           data_temp = data_temp4D(*,*,*,time_CS)
       endelse

       data_temp = data_temp * 1e9 ;v/v 2 ppbv
       data_temp = REVERSE(data_temp,3) ; reverse the 3rd dimension due to GCHP I/O issues
       data_CS[*,*,*,n] = data_temp
   endfor

   ncdf_close,fid ; always remember to close the netcdf file

end ; end of program
