; Purpose:
; Making benchmark plots given the data from two models.
;
; Function description:
; compare_multiTRC_layer: plot a number of tracers at one layer, given their data at that layer.  To be called by compare_multiTRC
; compare_multiTRC_zonal: plot a number of tracers' zonal profile, given their zonal profile data. To be called by compare_multiTRC
; compare_multiTRC: making various plots (layer, zonal..) of a number of tracers, given their data and names that are obtained by get_outputdata.pro.  To be called by GCHP_benchmark.pro
;
; Notes:
; Assume a 4x5x72L grid for zonal plot
; should change ctm_type( 'geosfp', res=4 )  to ctm_type( 'geosfp', res=2 ) if comparing a 2x2.5 run.

pro compare_multiTRC_layer,tracername,data1,name1,data2,name2,tag_process,tag_feature,outputdir

   Ntracer = N_elements(tracername)

   ; Number of rows & columns on the plot
   Rows = 3
   Cols = 3

   ; Use Postscript font
   !p.font = 0

   ; Number of colorbar tickmarks
   Divisions = ColorBar_NDiv( 6 )

   ; Open the plot device and initialize the page
   OutFileName=outputdir+tag_process+'_'+tag_feature+'.ps'
   open_device,/ps,/color,file=OutFileName
   ; Multiple panels per page
   MultiPanel, Rows=Rows, Cols=Cols, Margin=[ 0.03, 0.03, 0.03, 0.03 ]

   ; Loop over all tracers
   for n=0,Ntracer-1 do begin
       mindata = 0.0
       maxdata = max(data1[*,*,n])

       if maxdata gt 0.1 then begin
           unit='ppbv'
       endif else begin
           data1[*,*,n] = data1[*,*,n]*1e3 
           data2[*,*,n] = data2[*,*,n]*1e3 
           maxdata = maxdata*1e3 
           unit='pptv'
       endelse

       diffrange = max(abs(data1[*,*,n]-data2[*,*,n]))
       varlabel = 'no.'+strtrim(n+1,2)+' '+tracername[n]
       print,'plot '+varlabel

       myct,/WhGrYlRd
       tvmap,data1[*,*,n],$
           title=varlabel+'; '+name1,mindata=mindata,maxdata=maxdata,$
           /sample,/cbar,/grid,/continents,unit=unit

       tvmap,data2[*,*,n],$
           title=varlabel+'; '+name2,mindata=mindata,maxdata=maxdata,$
           /sample,/cbar,/grid,/continents,unit=unit

       myct,/diff
       tvmap,data2[*,*,n]-data1[*,*,n],$
           title=varlabel+'; '+name2+' - '+name1,mindata=-diffrange,maxdata=diffrange,$
           /sample,/cbar,/grid,/continents,unit=unit

       if n mod rows eq 0 then begin 
           ; Plot the top title on each page  
           XYoutS, 0.5, 1.03, 'GCHP benchmark: '+tag_process+' '+tag_feature, $
               /Normal, Color=!MYCT.BLACK, CharSize=1.0, Align=0.5
       endif

   endfor ;end loop for tracers

   ; Cancel previous MultiPanel Settings
   MultiPanel, /Off
   
   ; Close plot device
   Close_Device

end ; end of program



pro compare_multiTRC_zonal,tracername,xtitle,xlabel,ytitle,ylabel,data1,name1,data2,name2,tag_process,tag_feature,outputdir

   Ntracer = N_elements(tracername)

   ; Number of rows & columns on the plot
   Rows = 3
   Cols = 3

   ; Use Postscript font
   !p.font = 0

   ; Number of colorbar tickmarks
   Divisions = ColorBar_NDiv( 6 )

   ; Open the plot device and initialize the page
   OutFileName=outputdir+tag_process+'_'+tag_feature+'.ps'
   open_device,/ps,/color,file=OutFileName
   ; Multiple panels per page
   MultiPanel, Rows=Rows, Cols=Cols, Margin=[ 0.03, 0.03, 0.03, 0.03 ]

   ; Loop over all tracers
   for n=0,Ntracer-1 do begin
       mindata = 0.0
       maxdata = max(data1[*,*,n])

       if maxdata gt 0.1 then begin
           unit='ppbv'
       endif else begin
           data1[*,*,n] = data1[*,*,n]*1e3 
           data2[*,*,n] = data2[*,*,n]*1e3 
           maxdata = maxdata*1e3 
           unit='pptv'
       endelse

       diffrange = max(abs(data1[*,*,n]-data2[*,*,n]))
       varlabel = 'no.'+strtrim(n+1,2)+' '+tracername[n]
       print,'plot '+varlabel

       myct,/WhGrYlRd
       tvplot,data1[*,*,n],xlabel,ylabel,$
           title=varlabel+'; '+name1,mindata=mindata,maxdata=maxdata,$
           /sample,/cbar,unit=unit,xtitle=xtitle,ytitle=ytitle

       tvplot,data2[*,*,n],xlabel,ylabel,$
           title=varlabel+'; '+name2,mindata=mindata,maxdata=maxdata,$
           /sample,/cbar,unit=unit,xtitle=xtitle,ytitle=ytitle

       myct,/diff
       tvplot,data2[*,*,n]-data1[*,*,n],xlabel,ylabel,$
           title=varlabel+'; '+name2+' - '+name1,mindata=-diffrange,maxdata=diffrange,$
           /sample,/cbar,unit=unit,xtitle=xtitle,ytitle=ytitle

       if n mod rows eq 0 then begin 
           ; Plot the top title on each page  
           XYoutS, 0.5, 1.03, '2-model comparison: '+tag_process+' '+tag_feature, $
               /Normal, Color=!MYCT.BLACK, CharSize=1.0, Align=0.5
       endif

   endfor ;end loop for tracers

   ; Cancel previous MultiPanel Settings
   MultiPanel, /Off
   
   ; Close plot device
   Close_Device


end ; end of program



pro compare_multiTRC,tracername,data1,name1,data2,name2,tag,outputdir,plot_strat=plot_strat

   shape = size(data1)
   Nlon = shape[1]
   Nlat = shape[2]
   Nlev = shape[3]
   Ntracer = shape[4]

   ;====================================================================
   ; parameters 
   ;====================================================================
   L_surf = 0 ; IDL index starts from 0
   L_500hpa = 22
   L_trop = 25  ; the upper limit of tropospheric plot
   L_strat = 32 ; the bottom limit of stratospheric plot
   
   ;====================================================================
   ; plot surface 
   ;====================================================================

   ; IDL only trims the last dimension, so data1_surf will have the shape
   ; [Nlon,Nlat,1,Ntracer] without pre-defining dimension 
   data1_surf = fltarr(Nlon,Nlat,Ntracer)
   data1_surf[*,*,*] = data1[*,*,L_surf,*]
   data2_surf = fltarr(Nlon,Nlat,Ntracer)
   data2_surf[*,*,*] = data2[*,*,L_surf,*]

   compare_multiTRC_layer,tracername,$
       data1_surf,name1,data2_surf,name2,tag,'surface',outputdir
   
   ;====================================================================
   ; plot 500hpa  
   ;====================================================================
   data1_500hpa = fltarr(Nlon,Nlat,Ntracer)
   data1_500hpa[*,*,*] = data1[*,*,L_500hpa,*]
   data2_500hpa = fltarr(Nlon,Nlat,Ntracer)
   data2_500hpa[*,*,*] = data2[*,*,L_500hpa,*]

   compare_multiTRC_layer,tracername,$
       data1_500hpa,name1,data2_500hpa,name2,tag,'500hpa',outputdir

   ;====================================================================
   ; plot 180 degree longitude cross-section in troposphere
   ;====================================================================
   gridinfo = ctm_grid( ctm_type( 'geosfp', res=4 ) )
   lat = gridinfo.ymid
   pmid = gridinfo.pmid

   data1_180lon_t = fltarr(Nlat,L_trop+1,Ntracer)
   data1_180lon_t[*,*,*] = data1[0,*,0:L_trop,*]
   data2_180lon_t = fltarr(Nlat,L_trop+1,Ntracer)
   data2_180lon_t[*,*,*] = data2[0,*,0:L_trop,*]

   compare_multiTRC_zonal,tracername,'latitude',lat,'level',indgen(L_trop)+1,$
       data1_180lon_t,name1,data2_180lon_t,name2,tag,'180lon_trop',outputdir

   ;====================================================================
   ; plot 180 degree longitude cross-section in stratosphere
   ;====================================================================
   if keyword_set(plot_strat) then begin
       data1_180lon_s = fltarr(Nlat,Nlev-L_strat,Ntracer)
       data1_180lon_s[*,*,*] = data1[0,*,L_strat:Nlev-1,*]
       data2_180lon_s = fltarr(Nlat,Nlev-L_strat,Ntracer)
       data2_180lon_s[*,*,*] = data2[0,*,L_strat:Nlev-1,*]

       compare_multiTRC_zonal,tracername,'latitude',lat,'level',indgen(Nlev-L_strat)+L_strat+1,$
           data1_180lon_s,name1,data2_180lon_s,name2,tag,'180lon_strat',outputdir
    endif

   ;====================================================================
   ; plot zonal mean in troposphere
   ;====================================================================
   data1_zm_t = fltarr(Nlat,L_trop+1,Ntracer)
   data1_zm_t[*,*,*] = mean(data1[*,*,0:L_trop,*],1)
   data2_zm_t = fltarr(Nlat,L_trop+1,Ntracer)
   data2_zm_t[*,*,*] = mean(data2[*,*,0:L_trop,*],1)

   compare_multiTRC_zonal,tracername,'latitude',lat,'level',indgen(L_trop)+1,$
       data1_zm_t,name1,data2_zm_t,name2,tag,'zonalmean_trop',outputdir

   ;====================================================================
   ; plot zonal mean in troposphere
   ;====================================================================
   if keyword_set(plot_strat) then begin
       data1_zm_s = fltarr(Nlat,Nlev-L_strat,Ntracer)
       data1_zm_s[*,*,*] = mean(data1[*,*,L_strat:Nlev-1,*],1)
       data2_zm_s = fltarr(Nlat,Nlev-L_strat,Ntracer)
       data2_zm_s[*,*,*] = mean(data2[*,*,L_strat:Nlev-1,*],1)
       
       compare_multiTRC_zonal,tracername,'latitude',lat,'level',indgen(Nlev-L_strat)+L_strat+1,$
           data1_zm_s,name1,data2_zm_s,name2,tag,'zonalmean_strat',outputdir
    endif

   end ; end of program
