IDL scripts for GCHP benchmark ( comparing it with GC-classic )
It can also compare two versions of GC-classic or two versions of GCHP

J.W.Zhuang 2017/01

===============================================
1. Tool introduction

Only compare tracer mixing ratio fields because that's the only diagnostics currently available in GCHP.
Compare the mixing ratio of all tracers at:
[1] surface (the 1st level)
[2] 500hpa (the 23th level)
[3] zonal mean profile (in troposphere and stratosphere)
[4] cross section at 180 degree longitude  (in troposphere and stratosphere) 
The reason for choosing these plots is to mimic GC-classic' standard benchmark plots (gamap's map.pro and zonal.pro)

Also, calculate global and local error norms at surface, 500hpa and the whole domain.

The benchmark serves two purposes:
[1] Bug detection:  set up an idealized case with only one process on, look at the plots and see if anything goes wrong.
[2] Error quantification: Quantify the "expected" error due to the use of a different model grid and a lot of regridding, and other minor issues. The case can be either "realistic", just like the standard GEOS-Chem benchmark, or idealized, focusing on the error of a specific process.

===============================================
2. The structure of scripts

Compared to the standard benchmark scripts (e.g. gamap2/benchmark/maps.pro), the scripts here are less automated but more flexible and portable. Most importantly, reading data from files and plotting them are done by separate routines. Therefore, besides making standard plots, one can also manipulate data arrays (e.g. printing values, taking average) after reading the data in.

2.1. Utility scripts:
(those to be called, typically you don't need to modify them)
[1]get_outputdata.pro: get data from GCC and GCHP outputs, given the file names
[2]compare_multiTRC.pro: plot a number of tracers on a single PDF file (output in PostScript, converted by ps2pdf afterwards), given their data arrays and names (which are obtained by get_outputdata.pro). Containing routines for plotting one layer or one cross section, as well as a wrapped routine for plotting multiple features. 
[3]errornorm.pro: calculate standard error norms and print to file, given the data from two models (which are obtained by get_outputdata.pro)

2.2. Main scripts:
(those to be directly executed, the default one is just an example, you might need to change the filenames, tags, and loops) 
[1]GCHP_benchmark.pro: compare the GCHP's and GC-classic's simulation results
[2]geosfp_vs_merra2: compare two GC-classic runs with either geosfp or merra2. It turns out that the error introduced by CubeSphere<->LatLon regriding has the similar magnitude with that introduced by different metfield products. 

===============================================
3. How to use the scripts

Typical usage:
open IDL in this directory, enter:
.compile get_outputdata.pro
.compile compare_multiTRC.pro
.compile errornorm.pro
.compile GCHP_benchmark.pro
GCHP_BENCHMARK

then convert output PostScript to PDF:
./all_ps2pdf.sh

Input data files should be:
[1] GCC: ND49 time series containing multiple tracers 
[2] GCHP: instantaneous fields including multiple tracers, regridded to GCC's resolution
(I typically plot the first 66 tracers, but it works for any numbers of tracers. The routine will first extract all the tracers in GCC's ND49 diagnostics, and then try to find corresponding tracer names in GCHP's output file)

===============================================
4. Future Plan:

Will migrate to python after NetCDF diagnostics are available in GC-classic. Will not make effort to develop and optimize this IDL version.

