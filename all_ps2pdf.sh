#!/bin/bash

#cd outputfig/geosfp_vs_merra2/
cd outputfig/

for file in $(ls *.ps)
do
    echo convert ${file} to pdf
    ps2pdf ${file}
done
