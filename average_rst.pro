; taking layer-average of a default restart file for better examing column-physics

pro average_rst

;parameters
Nlon=72
Nlat=46
Nlev=72
Ntracer=66

file = '../restart/initial_GEOSChem_rst.4x5_uniform.nc' ;the file to modify
fID = NCDF_OPEN(file,/write)

;NCDF_VARGET, FID, 'TRC_NO', data ; this directly reads data by name
;but we need to go through all 66 tracers, from NO to HNO2, so read by id
for itracer=0,Ntracer-1 do begin
;for itracer=0,0 do begin
    varID = itracer+4 ; +4 to skip time,lev,lat,lon

    ; check variable's name
    varinfo = NCDF_VARINQ(fID, varID)
    print,itracer,': ',varinfo.Name

    ; read data by id
    NCDF_VARGET, fID, varID, data

    ; get level average, assign to the data array
    levmean = fltarr(Nlev)
    for ilev=0,Nlev-1 do begin
       levmean[ilev] = mean(data[*,*,ilev]) 
       data[*,*,ilev] = levmean[ilev]
    endfor
    ;print,'mean',levmean*1e9 ; v/v to ppbv

    ; write into file
    NCDF_VARPUT, fID, varID, data

endfor

NCDF_CLOSE, FID ; must close the file to actually make changes 

end ; end of program
