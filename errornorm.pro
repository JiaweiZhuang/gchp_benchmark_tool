; Purpose:
; Calculate standard error norm given the data from two models. Write to text files
;
; Function description:
; errornorm: calculate the global mass error (Lmass) and first-order error norm (L1), given two data arrays and weights. To be called by errornorm_multiTRC
; errornorm_multiTRC: calculate the error norm of all tracers, given their data and names that are obtained by get_outputdata.pro. To be called by GCHP_benchmark.pro
;
; Notes:
; Weight the mixing ratio by box_area*pressure thickness. Assume a 4x5x72L grid when calculating the weight.
; should change ctm_type( 'geosfp', res=4 )  to ctm_type( 'geosfp', res=2 ) if comparing a 2x2.5 run.

function errornorm,data1,data2,weight

errornorm=fltarr(2) ; Lmass,L1

errornorm[0] = total( (data2-data1)*weight )/total( data1*weight )
errornorm[1] = total( abs(data2-data1)*weight )/total( abs(data1)*weight ) 

return,errornorm*1e2 ; percentage

end; end of program

pro errornorm_multiTRC,tracername,data1,name1,data2,name2,tag,outputdir

   gridinfo = ctm_grid( ctm_type( 'geosfp', res=4 ) )
   delp = ts_diff(gridinfo.pedge,1)
   delp = delp[0:-2]

   dy = -ts_diff(sin(gridinfo.yedge/180*!PI),1)
   dy = dy[0:-2]

   dx = -ts_diff(gridinfo.xedge,1)
   dx = dx[0:-2]
   area = dx # dy

   weight = fltarr(gridinfo.IMX,gridinfo.JMX,gridinfo.LMX)
   for ilev=0,gridinfo.LMX-1 do begin
       weight[*,*,ilev] = area*delp[ilev]
   endfor

   L_surf = 0
   L_500hpa = 22
   Ntracer = n_elements(tracername)

   filename = outputdir+tag+'_errornorm.txt'
   openw, lun, filename, /Get_LUN, WIDTH=80
   printf,lun,'case-',tag,'  truth-',name1,'  experiment-',name2

   printf,lun,format='(A-17,A-16,A-16,A-16)',$
       ' ','surface','500hpa','global'

   printf,lun,format='(A-6,A-10,A-9,A-7,A-9,A-7,A-9,A-7)',$
       'no.','name','Lmass','L1','Lmass','L1','Lmass','L1'

   for n=0,Ntracer-1 do begin
       err_surf = errornorm(data1[*,*,L_surf,n],data2[*,*,L_surf,n],area)
       err_500hpa = errornorm(data1[*,*,L_500hpa,n],data2[*,*,L_500hpa,n],area)
       err_glob = errornorm(data1[*,*,*,n],data2[*,*,*,n],weight)
       printf,lun,format='(I-4,A-8,F8.2,"%",F6.2,"%",F8.2,"%",F6.2,"%",F8.2,"%",F6.2,"%")',$
           n+1,tracername[n],err_surf[0],err_surf[1],err_500hpa[0],err_500hpa[1],err_glob[0],err_glob[1]
   endfor

   free_lun, lun

   end; end of program
