;compare geosfp and merra2's simulation results

pro geosfp_vs_merra2 

process_list=['CloudConv','PBL','DryDep','WetDep','Chemistry']

dir_1 = '../geosfp_4x5_tropchem/outputdir/'
dir_2 = '../merra2_4x5_tropchem/outputdir/'
time = 249816 ; TAU0 in ND49 diagnostics

for n=0,4 do begin
    process = process_list[n] 
    file_1=dir_1+process+'.ts20130701.bpch'
    file_2=dir_2+process+'.ts20130701.bpch'
    
    getdata_LL,tracername,data_1,file_1,time
    getdata_LL,tracername,data_2,file_2,time ;tracername should not change in this second call

    tag=process
    outputdir='./outputfig/geosfp_vs_merra2/'
    compare_multiTRC,tracername,data_1,'geosfp',data_2,'merra2',tag,outputdir

    outputdir='./outputtxt/geosfp_vs_merra2/'
    errornorm_multiTRC,tracername,data_1,'geosfp',data_2,'merra2',tag,outputdir

endfor


end ; end of program
