;compare GCHP's and GC-classic's simulation results

pro GCHP_benchmark

; examine column physics by running with only process on
; use the name of the process as the prefix of GCC and GCHP's output filename
process_list=['CloudConv','PBL','DryDep','WetDep','Chemistry']

; We assume that rundirs are located in the parent directory of these IDL scripts
; change the path if needed
dir_CS = '../gchp_4x5_tropchem/OutputDir/'
dir_LL = '../geosfp_4x5_tropchem/outputdir/'
time_CS = 0 ; time slice in GCHP outputfile. starts from 0
time_LL = 249816 ; TAU0 in ND49 diagnostics

outputdir_fig='./outputfig/' ; the directory for output plots
outputdir_txt='./outputtxt/' ;the directory for output errornorms (in text)

for n=0,4 do begin ; it will take several minutes to finish this loop
    process = process_list[n] 
    file_CS=dir_CS+process+'.GCHP.regrid.20130702_0000z.nc4'
    file_LL=dir_LL+process+'.ts20130701.bpch'
    
    getdata_LL,tracername,data_LL,file_LL,time_LL ;tracername is an output array
    getdata_CS,tracername,data_CS,file_CS,time_CS ;tracername is an input array

    tag=process;use the process name as the title and filename of the plots

    ; making plots
    if strmatch(process,'*Chemistry*') then begin ;only plot the stratosphere for Chemistry benchmark
        compare_multiTRC,/plot_strat,tracername,data_LL,'GCclassic',data_CS,'GCHP',tag,outputdir_fig
    endif else begin
        compare_multiTRC,tracername,data_LL,'GCclassic',data_CS,'GCHP',tag,outputdir_fig
    endelse

    ; calculate error norms
    errornorm_multiTRC,tracername,data_LL,'GCclassic',data_CS,'GCHP',tag,outputdir_txt

endfor


end ; end of program
